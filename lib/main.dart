import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/basic.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  void _inc() {
    setState(() {
      _result++;
    });
  }

  void _dec() {
    setState(() {
      _result--;
    });
  }

  int _result = 0;

  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('My Counter App'),
          ),
          body: Center(
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('$_result', style: TextStyle(fontSize: 60),),
                    ),
                  ),
                ),
                Container(
                  height: 100,
                  color: Colors.red,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(20),
                        height: 50,
                        width: 50,
                        child: RaisedButton(
                          child: Text('-'),
                          onPressed: _dec,
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(),
                      ),
                      Container(
                        margin: EdgeInsets.all(20),
                        height: 50,
                        width: 50,
                        child: RaisedButton(
                          child: Text('+'),
                          onPressed: _inc,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}
